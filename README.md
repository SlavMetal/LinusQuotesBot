[![Requirements Status](https://requires.io/github/SlavMetal/LinusQuotesBot/requirements.svg?branch=master)](https://requires.io/github/SlavMetal/LinusQuotesBot/requirements/?branch=master)
# LinusQuotesBot
Simple Telegram bot that sends Linus Torvalds's quotes in different languages
## Features
* English and Russian languages
* Use of Redis to store language settings
* In groups, only admins are allowed to change bot's language
## Setup
1. Clone the repository:
    ```Bash
    git clone https://github.com/SlavMetal/LinusQuotesBot
    ```
1. Install required packages:
    ```Bash
    pip3 install -r requirements.txt
    ```
1. Replace the token in config.yml with the real one from [@BotFather](https://t.me/BotFather).
## Run
```Bash
python3 linusquote.py
```
## TODO
* [x] Add Russian language
* [x] Store language settings in DB
* [ ] Async
## Contact
You can contact me directly in [Telegram](https://t.me/SlavMetal).
